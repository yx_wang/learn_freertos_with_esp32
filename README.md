# Learning-FreeRTOS-with-esp32

#### 什么是 RTOS
其本质上是运行在小型嵌入式设备上的特殊软件-系统软件。如同手机的安卓系统软件、windows 系统软件。
##### RTOS VS 裸机系统
传统的裸机程序（无操作系统）其内部是一个前后台系统，其中前台程序主要是中断服务程序组成，管理系统标识，后台程序为一个主循环，通过一遍一遍地查询系统标识，执行对应的代码块：

![](images/c0_00_0.png)

在裸机系统中经常存在下面的问题：

case1: 触发 isr1 后就会执行 code2，即便 isr2 发生，也要先执行完 code2 ，再执行 code3，对 code3 的实时响应造成影响。

case2: 在执行 code3  时，若 isr1 发生，系统不能及时地切换到其他 code，需要重新轮询到对应的系统标识后才能执行对应的 code，CPU 没有及时用到重要的事情上。

因此，裸机系统的缺点是，CPU 利用率底，各个事件的实时性差、缺乏规范的通信组件、开发者必须以轮询、前后台响应的思路进行编程，往往需要彻底地了解整个系统才能写出合适的代码（与硬件的隔离性差），开发难度大。

**RTOS（Real Time Operating System， 实时操作系统简称*RTOS*）**，是一种通用的、规范的任务管理框架，用于控制任务的运行和任务之间、任务与中断之间的合作关系。通过 RTOS，程序可以兼容复杂的系统组件，用户也不必理解设备硬件的全部知识就可以快捷地调用设备的各种功能，实现系统集成。

RTOS 操作系统的**核心功能**是：自动管理 CPU 的使用权，最大化 CPU 的利用率，使得操作系统内的每个任务（进程\线程）都能在一定时间内及时的获取 CPU 的使用权，就像多个任务“同时在运行”一样：

![](images/c0_00_02.png)

以在手机上看小说和听音乐为例子，逻机系统更适合实现先看小说、再听音乐这种简单的逻辑，而带操作系统的设备则更容易实现边听音乐边看小说的逻辑。再以拍照+美颜为例，逻机系统更适合先拍照再把图片进行美颜的逻辑，而带操作系统的设备则更容易实现拍照时直接将美颜效果呈现在屏幕上的逻辑，因为后者可以使用RTOS提供的通信组件将数据实时地传递给美颜的处理程序。

##### RTOS 提供了哪些功能

RTOS 提供了任务管理、资源访问控制、消息通信、存储管理、低功耗管理等的功能：

![](images/c0_00_01.png)

通过 RTOS 将提供系统管理的功能，开发者只需调用相关的 API 就可以立即知道系统的情况，创建执行特定功能的代码、代码之间的通信、集成复杂的协议栈，开发复用性高的代码都将变得简单。

#### 为什么要学习 FreeRTOS
FreeRTOS 是非常流行的多任务操作系统。相比其他 RTOS  操作系统，FreeRTOS 具有用户多、免费、组件丰富的优点。通过学习这一典型的操作系统，可以融会贯通地掌握其他 RTOS 系统，对更高级的操作系统如 linux 的学习也是很有帮助的。

#### 为什么使用 ESP32

ESP32 便宜且功能丰富，除了可以学习标准的 FreeRTOS 功能外，乐鑫还开发了 FreeRTOS 的更多功能，如 RingBuffer，双核结构下 FreeRTOS 的使用等。除此之外，使用 ESP32 还可以学习各种外设和网络通信的知识。开源生态也不错，示例代码和开发人员众多。

#### 课程介绍
该课程以问题案例为核心，以实际开发需求为案例，循序渐进地讲述开发过程中可能面临哪些问题及需求。通过实践代码，掌握编程思想。目前规划的内容可分为下述几点：

- [RTSO 基础篇-了解 FreeRTOS 的核心-任务的创建与使用。](https://blog.csdn.net/wangyx1234/article/details/127217253)

- [RTSO 基础篇-任务间的同步与消息通信](https://blog.csdn.net/wangyx1234/article/details/127482275)

- [RTSO 基础篇-任务之间的共享资源及共享资源的保护策略](https://blog.csdn.net/wangyx1234/article/details/127719637)

- [RTOS 中断篇](https://blog.csdn.net/wangyx1234/article/details/128175086)

- [RTOS 时间管理篇](https://blog.csdn.net/wangyx1234/article/details/128308709)

- [RTOS 内存使用策略篇](https://blog.csdn.net/wangyx1234/article/details/128521717)

- [RTOS 系统篇 资源管理模式](https://blog.csdn.net/wangyx1234/article/details/128538760)

- [RTOS 系统篇 特殊任务](https://blog.csdn.net/wangyx1234/article/details/128538819)、[任务看门狗使用策略](https://blog.csdn.net/wangyx1234/article/details/129033543)

- [RTOS 下驱动开发示例](https://blog.csdn.net/wangyx1234/article/details/129112746)

- RTOS 下系统编程示例

#### 更新计划

普普通通打工入，准备日更至少一篇（共计约七十篇）。2022 年底前更新完毕。

写这些也是为了督促自己学习总结，和各位工程师同学互勉。

#### 使用说明
关于硬件：任意一款 ESP32 开发板+若干杜邦线即可。

关于软件：思考再三，还是使用 C 语言进行开发。作商用的项目，把设备的性能挖掘到极致，还得是 C 语言这把锋利的小刀。

#### 编译环境安装教程
本课程的示例均基于乐鑫开源的开发环境 ESP-IDF\v4.4 进行演示。关于编译环境的搭建，你可以参考：
1. [ESO-IDF 编程指南-Get Start](https://docs.espressif.com/projects/esp-idf/zh_CN/release-v4.4/esp32/get-started/index.html)

2. [ESP-IDF 国内 gitee 网站](https://gitee.com/EspressifSystems/esp-idf/tree/release%2Fv4.4/)

3. [ESP-IDF 论坛](https://www.esp32.com/)
   建议使用 ubuntu 的开发环境，因为 ubuntu 下编译的更快。

   建立开发环境后，向编译 hello_world 一样进入各个示例编译程序运行，并观察 log 信息就可以了。
#### 参考资源

1.  [FreeRTOS 官方开发者指南](https://www.freertos.org/features.html)
2.  [FreeRTOS 官方 API 说明文档](https://www.freertos.org/a00106.html)
3.  [乐鑫 FreeRTOS API 说明](https://docs.espressif.com/projects/esp-idf/en/release-v4.4/esp32/api-reference/system/freertos.html)、[新增特性说明](https://docs.espressif.com/projects/esp-idf/en/release-v4.4/esp32/api-reference/system/freertos_additions.html)
4.  [我的博客](https://blog.csdn.net/wangyx1234/article/details/127201095?spm=1001.2014.3001.5501)
5.  [我的代码](https://gitee.com/yx_wang/learn_freertos_with_esp32)

#### 获取代码
1. 点击代码仓库页面的**克隆/下载**按钮，下载压缩包获取代码更新：

   ![](images/c0_00_03.png)

   


2. 使用 git 获取代码更新
    如果你已经熟悉使用 git，可以执行下述命令获取仓库代码：

  `git clone https://gitee.com/yx_wang/learn_freertos_with_esp32`

后续你可以使用下述命令获取更新：

  `git pull origin master`

如果你还不熟悉如何使用 git，我建议你谷歌\百度一下如何安装 git，如何使用 git。熟悉 git 对于提高技术人员的技术水平非常有帮助，一开始你不需要了解 git 的方方面面，边用边学就可以了。
#### 学习建议
1. 学会利用网络，随时查看相关函数的说明、用法、注意事项。
2. 参考博客的说明进行思考，编译运行程序、并尝试改写程序验证自己的想法。
3. 如何使用 API 并不重要，认真体会每个组件、每个问题、每次改进所涉及的因与果，形成自己的编程心得。
4. 真正地掌握一个知识是需要努力和汗水的，极少有可以让你轻松掌握的知识。坚持看完整个示例，你的系统编程思维、C语言知识都会得到锻炼！！！
#### 注意事项
1. 在学习该课程前，请至少保证已经熟悉 C 语言的基本用法，如果没有请暂停课程的学习，先补充基本的C 语言知识，再开始学习。
2. 请确保你真的尝试了解过什么是 RTOS，比如百度或者谷歌它。
3. 建议你使用一块真的开发板来学习嵌入式相关的知识。

#### Liceance

 [Apache-2.0](https://gitee.com/yx_wang/Learning-FreeRTOS-operating-system-with-esp32/blob/master/LICENSE)，转载及复制需保留证书和作者。

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request