/**
 * Licensed under the Apache License, Version 2.0 http://www.apache.org/licenses/LICENSE-2.0
 * @brief FreeRTOS应用示例
 * @details
 * @author 王炸物联网https://blog.csdn.net/wangyx1234?spm=1000.2115.3001.5343
 * @date 2022
 * @version 1.0
 * @copyright 版权所有，禁止用于商业用途
 */
#include <stdio.h>
#include <string.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"

#include "driver/gpio.h"
#include "esp_rom_sys.h"

#define TASK_TAG1      "TASK1"
#define TASK_TAG2      "TASK2"
#define TASK_NUM       (2)
TaskHandle_t task1; // 任务1结构对象
TaskHandle_t task2; // 任务1结构对象  

static int task1_flag;
static int task2_flag;

static SemaphoreHandle_t sync_count_sem;
static SemaphoreHandle_t task_delete_sem;

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task1_process(void *arg)
{
    while (1) {
        task1_flag++; // 计数进入while 的次数
        if(xSemaphoreGive(sync_count_sem)!=pdTRUE) {
            printf("SEND err\r\n");
        }
        if(task1_flag > 5) {
            //Test complete, delete task
            xSemaphoreGive(task_delete_sem);
            break;
        }
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
    vTaskDelete(NULL);
}

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task2_process(void *arg)
{
    static const char *TASK2_TAG = TASK_TAG2;
    while (1) {
        task2_flag++; // 计数进入while 的次数
        xSemaphoreTake(sync_count_sem, portMAX_DELAY);
        printf("%s: task1_flag1 = %d, task2_flag=%d\r\n",TASK2_TAG, task1_flag, task2_flag);
        if(task2_flag > 5) {
            //Test complete, delete task
            xSemaphoreGive(task_delete_sem);
            break;
        }
        vTaskDelay(pdMS_TO_TICKS(2000));
    }
    vTaskDelete(NULL);
}

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task3_process(void *arg)
{
    for(int k = 0; k < TASK_NUM; k++){             //Wait for sender and receiver task deletion
        xSemaphoreTake(task_delete_sem, portMAX_DELAY);
    }
    vTaskDelay(5);      //Give time to delete
    printf("All Done\r\n");
    vSemaphoreDelete(sync_count_sem);
    vSemaphoreDelete(task_delete_sem);
    vTaskDelete(NULL);
}

static void init(void) {
    printf("Hello world!\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is %s chip with %d CPU core(s), WiFi%s%s, ",
            CONFIG_IDF_TARGET,
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("Minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size());

    sync_count_sem = xQueueCreateCountingSemaphore(3, 0);
    if (sync_count_sem == NULL) {
        printf("sem created fail\n");
    }

    task_delete_sem = xQueueCreateCountingSemaphore(TASK_NUM, 0);
    if (task_delete_sem == NULL) {
        printf("sem created err\n");
    }
}

void app_main(void)
{
    init();
    /*
    static BaseType_t xTaskCreate(TaskFunction_t pvTaskCode, const char *constpcName, const uint32_t usStackDepth, 
    void *constpvParameters, UBaseType_t uxPriority, TaskHandle_t *constpxCreatedTask)*/
    xTaskCreate(&task1_process, "task1", 1024*2, (void *)"1", configMAX_PRIORITIES-3, &task1); // configMAX_PRIORITIES = 25
    xTaskCreate(&task2_process, "task2", 1024*2, (void *)"2", configMAX_PRIORITIES-2, &task2); // configMAX_PRIORITIES = 25
    xTaskCreate(&task3_process, "task3", 1024*2, (void *)"3", configMAX_PRIORITIES-4, NULL); // configMAX_PRIORITIES = 25
}
