#ifndef BUTTON_H
#define BUTTON_H

#include <stdio.h>
#include <stdint.h>

#define BUTTON_MSG_NR                   10          // 按键消息缓存数量
#define BUTTON_SCAN_INTERVAL            20          // 安键扫描的时间间隔, ms为单位
#define BUTTON_ID_OFFSET               1
#define ButtonNone                     -1

typedef enum {
    button_priority_high,
    button_priority_low,
} button_priority_t;

typedef enum {
    ButtonDown,
    ButtonUp,
} button_state_t;

typedef int (* button_cb_t)(void *, void *);
typedef void *button_handle_t;

typedef struct __iot_button_t {
    uint32_t button_gpio_num;
    button_cb_t button_cb;
    button_priority_t priority;
} iot_button_t;

button_handle_t button_create(iot_button_t button_config);
esp_err_t button_delete(button_handle_t btn_handle);
int printf_register_button_gpio_num(void);

#if EN_DEBUG_PRINT == 0
#define DEBUG_PRINT(str, arg)
#else
#define DEBUG_PRINT(str, arg) printf(str, arg)
#endif

#endif //BUTTON_H
