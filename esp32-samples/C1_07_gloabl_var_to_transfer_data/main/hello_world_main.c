/**
 * Licensed under the Apache License, Version 2.0 http://www.apache.org/licenses/LICENSE-2.0
 * @brief FreeRTOS应用示例
 * @details
 * @author 王炸物联网https://blog.csdn.net/wangyx1234?spm=1000.2115.3001.5343
 * @date 2022
 * @version 1.0
 * @copyright 版权所有，禁止用于商业用途
 */
#include <stdio.h>
#include <string.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"

#include "driver/gpio.h"
#include "esp_rom_sys.h"

#define TASK_TAG1      "TASK1"
#define TASK_TAG2      "TASK2"
#define ARRAY_SIZE     (3)

TaskHandle_t task1; // 任务1结构对象
TaskHandle_t task2; // 任务1结构对象   

static int task1_flag;
static int task2_flag;

static SemaphoreHandle_t sem1;
static uint32_t global_array[ARRAY_SIZE];

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task1_process(void *arg)
{
    static const char *TASK1_TAG = TASK_TAG1;
    uint32_t temp;
    while (1) {
        task1_flag++;
        temp = task1_flag%ARRAY_SIZE;
        global_array[temp] = task1_flag;
        if(!temp) {
            xSemaphoreGive(sem1);
        }
        
        vTaskDelay(pdMS_TO_TICKS(500));
    }
}

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task2_process(void *arg)
{
    static const char *TASK2_TAG = TASK_TAG2;
    while (1) {
        xSemaphoreTake(sem1, portMAX_DELAY);

        printf("%s: get data=%d\t", TASK2_TAG, global_array[0]);
        vTaskDelay(pdMS_TO_TICKS(500)); // 模拟该任务执行过程中，被抢占 CPU 的情况

        printf("%d\t", global_array[1]);
        vTaskDelay(pdMS_TO_TICKS(1500));// 模拟该任务执行过程中，被抢占 CPU 的情况

        printf("%d\n", global_array[2]);

        task2_flag++;
    }
}

static void init(void) {
    printf("Hello world!\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is %s chip with %d CPU core(s), WiFi%s%s, ",
            CONFIG_IDF_TARGET,
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("Minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size());

   sem1 = xSemaphoreCreateBinary();
   
    if (sem1 == NULL) {
        printf("sem created fail\n");
    } 
}

void app_main(void)
{
    init();
    /*
    static BaseType_t xTaskCreate(TaskFunction_t pvTaskCode, const char *constpcName, const uint32_t usStackDepth, 
    void *constpvParameters, UBaseType_t uxPriority, TaskHandle_t *constpxCreatedTask)*/
    xTaskCreate(&task1_process, "task1", 1024*2, (void *)"1", configMAX_PRIORITIES-1, &task1); // configMAX_PRIORITIES = 25
    xTaskCreate(&task2_process, "task2", 1024*2, (void *)"2", configMAX_PRIORITIES-2, &task2); // configMAX_PRIORITIES = 25
}

