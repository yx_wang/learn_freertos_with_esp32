/**
 * @brief FreeRTOS应用示例
 * @details
 * @author 王炸物联网https://blog.csdn.net/wangyx1234?spm=1000.2115.3001.5343
 * @date 2022
 * @version 1.0
 * @copyright 版权所有，禁止用于商业用途
 */
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/stream_buffer.h"
#include "freertos/message_buffer.h"

#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"

#include "driver/gpio.h"
#include "esp_rom_sys.h"

#define TASK_TAG1      "TASK1"
#define TASK_TAG2      "TASK2"
#define CUSTOM_TRIGGER_LEVEL (5)

TaskHandle_t task1; // 任务1结构对象
TaskHandle_t task2; // 任务1结构对象  

static uint8_t task1_flag;
static uint8_t task2_flag;

static StreamBufferHandle_t sb;
SemaphoreHandle_t end_test;

/*display buffer in hex on a line*/
void disp_buf(const char* TAG, uint8_t* buf, uint32_t len)
{
    int i;
    assert(buf != NULL);
    printf("%s: The buffer data is as follows:", TAG);
    for (i = 0; i < len; i++) {
        printf("%02x ", buf[i]);
        if ((i + 1) % 16 == 0) {
            printf("\n");
        }
    }
    printf("\n");
}

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task1_process(void *arg)
{
    static const char *TASK1_TAG = TASK_TAG1;
    BaseType_t result;
    while (1) {
        printf("%s: flag=%d\n", TASK1_TAG, task1_flag);
        result = xStreamBufferSend(sb, &task1_flag, 1, 0);
        if(!result) {
            continue;
        }

        task1_flag++;
        if(task1_flag > 22) {
            xSemaphoreGive(end_test);
            vTaskDelete(NULL);
        }
        
        vTaskDelay(pdMS_TO_TICKS(500));
    }
}

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task2_process(void *arg)
{
    static const char *TASK2_TAG = TASK_TAG2;
    uint8_t read_byte[CUSTOM_TRIGGER_LEVEL];
    memset(read_byte, 0xFF, sizeof(read_byte));
    uint32_t wait_time = 6000;

    while (1) {
        uint32_t read_count = xStreamBufferReceive(sb, &read_byte, CUSTOM_TRIGGER_LEVEL, pdMS_TO_TICKS(wait_time));
        if(read_count > 0) {
            if(read_count == CUSTOM_TRIGGER_LEVEL) {
                printf("%s: read triggle level bytes\r\n",TASK2_TAG);
                disp_buf(TASK2_TAG, read_byte, read_count);
            } else {
                printf("%s: timeout and read some data\r\n",TASK2_TAG);
                disp_buf(TASK2_TAG, read_byte, read_count);
            }
        } else {
            printf("%s: read err\r\n",TASK2_TAG);
        }

        if(xSemaphoreTake(end_test, 0)) {
            vStreamBufferDelete(sb);
            vSemaphoreDelete(end_test);
            vTaskDelete(NULL);
        }

        task2_flag++;
    }
}

static void init(void) {
    printf("Hello world!\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is %s chip with %d CPU core(s), WiFi%s%s, ",
            CONFIG_IDF_TARGET,
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("Minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size());

   sb = xStreamBufferCreate(256, CUSTOM_TRIGGER_LEVEL);
   end_test = xSemaphoreCreateBinary();

    if ((sb == NULL) || (end_test == NULL)) {
        printf("sb or sem created fail\n");
    }
    xStreamBufferReset(sb); // call this func before using.
}

void app_main(void)
{
    init();
    /*
    static BaseType_t xTaskCreate(TaskFunction_t pvTaskCode, const char *constpcName, const uint32_t usStackDepth, 
    void *constpvParameters, UBaseType_t uxPriority, TaskHandle_t *constpxCreatedTask)*/
    xTaskCreatePinnedToCore(&task1_process, "task1", 1024*2, (void *)"1", configMAX_PRIORITIES-1, &task1, 0); // configMAX_PRIORITIES = 25
    xTaskCreatePinnedToCore(&task2_process, "task2", 1024*2, (void *)"2", configMAX_PRIORITIES-2, &task2, 1); // configMAX_PRIORITIES = 25
}
