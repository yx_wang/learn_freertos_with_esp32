/**
 * Licensed under the Apache License, Version 2.0 http://www.apache.org/licenses/LICENSE-2.0
 * @brief FreeRTOS应用示例
 * @details
 * @author 王炸物联网https://blog.csdn.net/wangyx1234?spm=1000.2115.3001.5343
 * @date 2022
 * @version 1.0
 * @copyright 版权所有，禁止用于商业用途
 */
#include <stdio.h>
#include <string.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"

#include "driver/gpio.h"
#include "esp_rom_sys.h"

#define TASK_TAG1      "TASK1"
#define TASK_TAG2      "TASK2"
#define TASK_TAG3      "TASK3"
#define ARRAY_SIZE     (5)

TaskHandle_t task1; // 任务1结构对象
TaskHandle_t task2; // 任务1结构对象  

static int task1_flag;
static int task2_flag;

typedef struct custom_data {
    int array[ARRAY_SIZE];
}custom_data_t;

static QueueHandle_t custom_queue;

/*display buffer in hex on a line*/
void disp_buf(const char* TAG, int* buf, uint32_t len)
{
    int i;
    assert(buf != NULL);
    printf("%s: The buffer data is as follows:", TAG);
    for (i = 0; i < len; i++) {
        printf("%02x ", buf[i]);
        if ((i + 1) % ARRAY_SIZE == 0) {
            printf("\n");
        }
    }
    printf("\n");
}

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task1_process(void *arg)
{
    static const char *TASK1_TAG = TASK_TAG1;
    int remainder = 0;
    custom_data_t data_send = {0};
    while(1) {
        remainder = task1_flag%ARRAY_SIZE;
        if(remainder == 0) {
            if (xQueueSendToFront(custom_queue, (void *)&data_send, 0) != pdTRUE) { // 每五次发送一个数据
                printf("%s: failed send\n", TASK1_TAG); // 检查是否发送失败
            }
        }
        data_send.array[remainder] = task1_flag;

        task1_flag++;

        vTaskDelay(pdMS_TO_TICKS(500));
    }
}

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task2_process(void *arg)
{
    static const char *TASK2_TAG = TASK_TAG2;
    custom_data_t data_recv = {0};
    while (1) {
       if(xQueueReceive(custom_queue, &data_recv, portMAX_DELAY) == pdTRUE) {
        disp_buf(TASK2_TAG, &data_recv.array[0], ARRAY_SIZE);
        // printf("Now, task1_flag=%d\r\n", task1_flag);
       } 

        task2_flag++;
        
        vTaskDelay(pdMS_TO_TICKS(100));
    }
}

static void init(void) {
    printf("Hello world!\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is %s chip with %d CPU core(s), WiFi%s%s, ",
            CONFIG_IDF_TARGET,
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("Minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size());

   custom_queue = xQueueCreate(3, sizeof(custom_data_t));

    if (custom_queue== NULL) {
        printf("queue created fail\n");
    } 
}

void app_main(void)
{
    init();
    /*
    static BaseType_t xTaskCreate(TaskFunction_t pvTaskCode, const char *constpcName, const uint32_t usStackDepth, 
    void *constpvParameters, UBaseType_t uxPriority, TaskHandle_t *constpxCreatedTask)*/
    xTaskCreate(&task1_process, "task1", 1024*2, (void *)"1", configMAX_PRIORITIES-2, &task1); // configMAX_PRIORITIES = 25
    xTaskCreate(&task2_process, "task2", 1024*2, (void *)"2", configMAX_PRIORITIES-1, &task2); // configMAX_PRIORITIES = 25
}

