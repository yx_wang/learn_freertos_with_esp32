/**
 * Licensed under the Apache License, Version 2.0 http://www.apache.org/licenses/LICENSE-2.0
 * @brief FreeRTOS应用示例
 * @details
 * @author 王炸物联网https://blog.csdn.net/wangyx1234?spm=1000.2115.3001.5343
 * @date 2022
 * @version 1.0
 * @copyright 版权所有，禁止用于商业用途
 */
#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"

#define STACK_SIZE 3072


TaskHandle_t task1; // 任务1结构对象
static StackType_t xStack[STACK_SIZE];
static StaticTask_t xTaskBuffer; // Structure that will hold the TCB of the task being created.

static int task1_flag;           // 用于指示任务运行状态的标志变量

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task1_process(void *arg)
{
    while (1) {
        printf("task1_flag = %d\r\n", task1_flag);
        task1_flag++;
        vTaskDelay(pdMS_TO_TICKS(1000));
    }
}

static void init(void) {
    printf("Hello world!\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is %s chip with %d CPU core(s), WiFi%s%s, ",
            CONFIG_IDF_TARGET,
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("Minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size());
}

void app_main(void)
{
    init();
    /*
    static BaseType_t xTaskCreate(TaskFunction_t pvTaskCode, const char *constpcName, const uint32_t usStackDepth, 
    void *constpvParameters, UBaseType_t uxPriority, TaskHandle_t *constpxCreatedTask)*/
    xTaskCreateStatic(
                   (TaskFunction_t)&task1_process,     // Function that implements the task.
                   "task1",           // Text name for the task.
                   STACK_SIZE,        // Stack size in bytes, not words.
                   ( void * ) 1,      // Parameter passed into the task.
                   2,  // Priority at which the task is created.
                   xStack,            // Array to use as the task's stack.
                   &xTaskBuffer);    // Variable to hold the task's data structure.
}
