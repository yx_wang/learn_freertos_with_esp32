#ifndef BUTTON_H
#define BUTTON_H

#include <stdio.h>
#include <stdint.h>

#define BUTTON_MSG_NR                   10          // 按键消息缓存数量
#define BUTTON_SCAN_INTERVAL            20          // 安键扫描的时间间隔, ms为单位

typedef enum {
    ButtonNone,
    ButtonStartStop,
    Button1,
}Button_Id;

typedef enum {
    ButtonDown,
    ButtonUp,
}Button_State;

void ButtonInit (void);
uint32_t ButtonWaitPress (Button_Id * id);

#if EN_DEBUG_PRINT == 0
#define DEBUG_PRINT(str, arg)
#else
#define DEBUG_PRINT(str, arg) printf(str, arg)
#endif

#endif //BUTTON_H
