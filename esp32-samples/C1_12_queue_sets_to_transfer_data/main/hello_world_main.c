/**
 * @brief FreeRTOS应用示例
 * @details
 * @author 王炸物联网https://blog.csdn.net/wangyx1234?spm=1000.2115.3001.5343
 * @date 2022
 * @version 1.0
 * @copyright 版权所有，禁止用于商业用途
 */
#include <stdio.h>
#include <string.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "freertos/semphr.h"
#include "freertos/queue.h"
#include "freertos/event_groups.h"

#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"

#include "driver/gpio.h"
#include "esp_rom_sys.h"

#define TASK_TAG1      "TASK1"
#define TASK_TAG2      "TASK2"
#define TASK_TAG3      "TASK3"
/*
 * Basic queue set tests. Multiple queues are added to a queue set then each
 * queue is filled in a sequential order. The members returned from the queue
 * set must adhered to the order in which the queues were filled.
 */
#define NO_OF_QUEUES            2
#define QUEUE_LEN               4
#define ITEM_SIZE               sizeof(int)

TaskHandle_t task1; // 任务1结构对象
TaskHandle_t task2; // 任务1结构对象  
TaskHandle_t task3; // 任务1结构对象  

static int task1_flag;
static int task2_flag;
static int task3_flag;

static QueueHandle_t handles[NO_OF_QUEUES];
static QueueSetHandle_t set_handle;

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task1_process(void *arg)
{
    static const char *TASK1_TAG = TASK_TAG1;
    while (1) {
        printf("%s: send=%d\n", TASK1_TAG, task1_flag);
        if(xQueueSendToBack(handles[0], &task1_flag, portMAX_DELAY) != pdTRUE) {
            printf("%s: failed to send\n", TASK1_TAG);
        }

        task1_flag++;
        
        vTaskDelay(pdMS_TO_TICKS(1500));
    }
}

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task2_process(void *arg)
{
    static const char *TASK2_TAG = TASK_TAG2;
    while (1) {
        printf("%s: send=%d\n", TASK2_TAG, task2_flag);
        if(xQueueSendToBack(handles[1], &task2_flag, portMAX_DELAY) != pdTRUE) {
            printf("%s: failed to send\n", TASK2_TAG);
        }

        task2_flag++;
        
        vTaskDelay(pdMS_TO_TICKS(1500));
    }
}

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task3_process(void *arg)
{
    static const char *TASK3_TAG = TASK_TAG3;
    int item = 0;
    while (1) {
        QueueSetMemberHandle_t member = xQueueSelectFromSet(set_handle, portMAX_DELAY);
        if (member == handles[0]) {
            xQueueReceive((QueueHandle_t)member, &item, 0);
            printf("%s: recv queue1 and value=%d\n", TASK3_TAG, item);
        } else if (member == handles[1]) {
            xQueueReceive((QueueHandle_t)member, &item, 0);
            printf("%s: recv queue2 and value=%d\n", TASK3_TAG, item);
        } else {
            printf("%s: No support queue\n", TASK3_TAG);
        }
        
        if(task3_flag > 3) {
            xQueueRemoveFromSet(handles[0], set_handle);
        }

        task3_flag++;
    }
}

static void init(void) {
    printf("Hello world!\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is %s chip with %d CPU core(s), WiFi%s%s, ",
            CONFIG_IDF_TARGET,
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("Minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size());

   //Create queue set, queues, and add queues to queue set
    set_handle = xQueueCreateSet(NO_OF_QUEUES * QUEUE_LEN);
    for (int i = 0; i < NO_OF_QUEUES; i++) {
        handles[i] = xQueueCreate(QUEUE_LEN, ITEM_SIZE);
        assert(handles[i] != NULL);
        assert(xQueueAddToSet(handles[i], set_handle) == pdPASS);
    }
}

void app_main(void)
{
    init();
    /*
    static BaseType_t xTaskCreate(TaskFunction_t pvTaskCode, const char *constpcName, const uint32_t usStackDepth, 
    void *constpvParameters, UBaseType_t uxPriority, TaskHandle_t *constpxCreatedTask)*/
    xTaskCreatePinnedToCore(&task1_process, "task1", 1024*2, (void *)"1", configMAX_PRIORITIES-1, &task1, 0); // configMAX_PRIORITIES = 25
    xTaskCreatePinnedToCore(&task2_process, "task2", 1024*2, (void *)"2", configMAX_PRIORITIES-2, &task2, 1); // configMAX_PRIORITIES = 25
    xTaskCreatePinnedToCore(&task3_process, "task3", 1024*2, (void *)"3", configMAX_PRIORITIES-3, &task3, 1); // configMAX_PRIORITIES = 25
}

