#ifndef BUTTON_H
#define BUTTON_H

#include <stdio.h>
#include <stdint.h>

#define BUTTON_MSG_NR                   10          // 按键消息缓存数量
#define BUTTON_SCAN_INTERVAL            20          // 安键扫描的时间间隔, ms为单位
#define BUTTON_ID_OFFSET               1

typedef enum {
    ButtonNone,
    ButtonStartStop,
    Button1,
} button_id_t;

typedef enum {
    ButtonDown,
    ButtonUp,
} button_state_t;

typedef int (* button_cb_t)(void *, void *);

typedef struct __custom_button_t {
    uint32_t button_gpio_num;
    button_id_t button_id;
    button_cb_t button_cb;
} custom_button_t;

void ButtonInit (void);
uint32_t ButtonWaitPress (button_id_t * id);

#if EN_DEBUG_PRINT == 0
#define DEBUG_PRINT(str, arg)
#else
#define DEBUG_PRINT(str, arg) printf(str, arg)
#endif

#endif //BUTTON_H
