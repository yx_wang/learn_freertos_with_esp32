/**
 * Licensed under the Apache License, Version 2.0 http://www.apache.org/licenses/LICENSE-2.0
 * @brief FreeRTOS应用示例
 * @details
 * @author 王炸物联网https://blog.csdn.net/wangyx1234?spm=1000.2115.3001.5343
 * @date 2022
 * @version 1.0
 * @copyright 版权所有，禁止用于商业用途
 */
#include <stdio.h>
#include "sdkconfig.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"

#include "esp_system.h"
#include "esp_spi_flash.h"
#include "esp_log.h"
#include "esp_rom_sys.h"

TaskHandle_t task1; // 任务1结构对象
TaskHandle_t task2;
TaskHandle_t task3;

static int task1_flag;           // 用于指示任务运行状态的标志变量
static int task2_flag;           
static int task3_flag;   

#define TICKS_TO_DELAY 100
#define TICK_RATE   configTICK_RATE_HZ
#define TICK_PERIOD_US (1000000/TICK_RATE)
#define IDEAL_DELAY_PERIOD_MS  ((1000*TICKS_TO_DELAY)/TICK_RATE)
#define TICKS_TO_MS(x)  (((x)*1000)/TICK_RATE)

/**
 * 任务的运行代码
 * @param param 任务初始运行参数
 */
static void task1_process(void *arg)
{
    static const char *TASK1_TAG = "TASK1";
    TickType_t last_wake_time = 0;
    TickType_t ticks_before_delay = 0;
    ticks_before_delay = xTaskGetTickCount();
    while (1) {
        esp_rom_delay_us(100*1000); // 模拟有其他任务或者系统中断抢占 CPU，导致延迟进入 vtaskdelay()
        task1_flag++;
        vTaskDelay(pdMS_TO_TICKS(1000));
        last_wake_time = xTaskGetTickCount();
        printf("%s: tick used=%dms\r\n", TASK1_TAG, TICKS_TO_MS(last_wake_time-ticks_before_delay));
        ticks_before_delay = last_wake_time;
    }
}

static void task2_process(void *arg)
{
    static const char *TASK2_TAG = "TASK2";
    TickType_t ticks_before_delay;;
    TickType_t last_wake_time = 0;
    last_wake_time = xTaskGetTickCount();
    ticks_before_delay = last_wake_time;

    while (1) {
        esp_rom_delay_us(100*1000); // 模拟有其他任务或者系统中断抢占 CPU
        task2_flag++;
        vTaskDelayUntil(&last_wake_time, pdMS_TO_TICKS(1000)); // 注意，使用后，last_wake_time 的值会被更新为当下的系统 systick.
        printf("%s: tick used=%dms\r\n", TASK2_TAG, TICKS_TO_MS(xTaskGetTickCount() - ticks_before_delay));
        ticks_before_delay = last_wake_time;
    }
    vTaskDelete(NULL);
}

static void task3_process(void *arg)
{
    static const char *TASK3_TAG = "TASK3";
    while (1) {
        uint64_t total_time = esp_timer_get_time();
        vTaskDelay(100); // 延时 100 个 Tick，不一定延时了 100ms.
        total_time = esp_timer_get_time() - total_time;
        printf("%s: use time=%lldus\r\n", TASK3_TAG, total_time);
    }
}

static void init(void) {
    printf("Hello world!\n");

    /* Print chip information */
    esp_chip_info_t chip_info;
    esp_chip_info(&chip_info);
    printf("This is %s chip with %d CPU core(s), WiFi%s%s, ",
            CONFIG_IDF_TARGET,
            chip_info.cores,
            (chip_info.features & CHIP_FEATURE_BT) ? "/BT" : "",
            (chip_info.features & CHIP_FEATURE_BLE) ? "/BLE" : "");

    printf("Minimum free heap size: %d bytes\n", esp_get_minimum_free_heap_size());
}

void app_main(void)
{
    init();
    /*
    static BaseType_t xTaskCreate(TaskFunction_t pvTaskCode, const char *constpcName, const uint32_t usStackDepth, 
    void *constpvParameters, UBaseType_t uxPriority, TaskHandle_t *constpxCreatedTask)*/
    xTaskCreate(&task1_process, "task1", 1024*2, (void *)"1", configMAX_PRIORITIES, &task1); // configMAX_PRIORITIES = 25
    xTaskCreatePinnedToCore(&task2_process, "task2", 1024*2, (void *)"2", configMAX_PRIORITIES-2, &task2, 0);
    xTaskCreatePinnedToCore(&task3_process, "task3", 1024*2, (void *)"3", configMAX_PRIORITIES-3, &task3, 0);
}
